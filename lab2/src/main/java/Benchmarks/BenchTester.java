package Benchmarks;

import Classes.*;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;
import org.openjdk.jmh.annotations.*;
import java.util.concurrent.ConcurrentHashMap;

public class BenchTester {
    static int min = 50;

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    public void ArrayListRep100add() {
        ArrayListRep<Integer> r1 = new ArrayListRep<>();
        for (int i=0; i<min; i++)
        {
            r1.add(i);
        }

    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    public void ArrayListRep100contain() {
        ArrayListRep<Integer> r1 = new ArrayListRep<>();
        for (int i=0; i<min; i++)
        {
            r1.contains(i);
        }

    }
    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 5)
    public void ArrayListRep100remove() {
        ArrayListRep<Integer> r1 = new ArrayListRep<>();
        for (int i=0; i<min; i++)
        {
            r1.remove(i);
        }

    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void ConcurrentHashMapRep100add() {
        ConcurrentHashMapRep<Integer> chm = new ConcurrentHashMapRep<>();
        for (int i=0; i<min; i++) {
            chm.add(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void ConcurrentHashMapRep100contain() {
        ConcurrentHashMapRep<Integer> chm = new ConcurrentHashMapRep<>();
        for (int i=0; i<min; i++) {
            chm.contains(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void ConcurrentHashMapRep100remove() {
        ConcurrentHashMapRep<Integer> chm = new ConcurrentHashMapRep<>();
        for (int i=0; i<min; i++) {
            chm.remove(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void EclipseSetRep100add() {
        EclipseSetRep<Integer> set = new EclipseSetRep();
        for (int i=0; i<min; i++) {
            set.add(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void EclipseSetRep100contain() {
        EclipseSetRep<Integer> set = new EclipseSetRep();
        for (int i=0; i<min; i++) {
            set.contains(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void EclipseSetRep100remove() {
        EclipseSetRep<Integer> set = new EclipseSetRep();
        for (int i=0; i<min; i++) {
            set.remove(i);}
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void FastUtilObjectArrayListRes100add() {
        FastUtilObjectArrayListRep<Integer> set = new FastUtilObjectArrayListRep();
        for (int i=0; i<min; i++) {
            set.add(i);
        }
    }
    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void FastUtilObjectArrayListRes100contain() {
        FastUtilObjectArrayListRep<Integer> set = new FastUtilObjectArrayListRep();
        for (int i=0; i<min; i++) {
            set.contains(i);
        }
    }
    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void FastUtilObjectArrayListRes100remove() {
        FastUtilObjectArrayListRep<Integer> set = new FastUtilObjectArrayListRep();
        for (int i=0; i<min; i++) {
            set.remove(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void HashSetRes100add() {
        HashSetRep<Integer> set = new HashSetRep();
        for (int i=0; i<min; i++) {
            set.add(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void HashSetRes100contain() {
        HashSetRep<Integer> set = new HashSetRep();
        for (int i=0; i<min; i++) {
            set.contains(i);
        }
    }
    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void HashSetRes100remove() {
        HashSetRep<Integer> set = new HashSetRep();
        for (int i=0; i<min; i++) {
            set.remove(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void TreeSeRes100add() {
        TreeSetRep<Integer> set = new TreeSetRep();
        for (int i=0; i<min; i++) {
            set.add(i);
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void TreeSeRes100contain() {
        TreeSetRep<Integer> set = new TreeSetRep();
        for (int i=0; i<min; i++) {
            set.contains(i);
        }
    }
    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 4)
    @Measurement(iterations = 2)
    public void TreeSeRes100remove() {
        TreeSetRep<Integer> set = new TreeSetRep();
        for (int i=0; i<min; i++) {
            set.remove(i);
        }
    }


}
