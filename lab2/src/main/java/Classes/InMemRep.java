package Classes;

public interface InMemRep<T> {

    void add(T e);
    boolean contains(T e);
    void remove(T e);
}
