package Classes;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;


public class FastUtilObjectArrayListRep<T> implements InMemRep<T> {
    private ObjectArrayList<T> list;

    public FastUtilObjectArrayListRep() {list = new ObjectArrayList<>();}

    @Override
    public void add(T e) {
        list.add(e);
    }

    @Override
    public boolean contains(T e) {
        return list.contains(e);
    }

    @Override
    public void remove(T e) {
        list.remove(e);
    }
}
