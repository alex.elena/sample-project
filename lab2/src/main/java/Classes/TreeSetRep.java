package Classes;

import java.util.TreeSet;

public class TreeSetRep<T> implements InMemRep<T> {
    private TreeSet<T> tree;

    public TreeSetRep() {tree = new TreeSet<>();}

    @Override
    public void add(T e) {
        tree.add(e);
    }

    @Override
    public boolean contains(T e) {
        return tree.contains(e);
    }

    @Override
    public void remove(T e) {
        tree.remove(e);
    }
}
