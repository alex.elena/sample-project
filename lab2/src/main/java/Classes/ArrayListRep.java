package Classes;

import java.util.ArrayList;

public class ArrayListRep<T> implements InMemRep<T> {
    private ArrayList<T> list;

    public ArrayListRep() {list = new ArrayList<>();}

    @Override
    public void add(T e) {
        list.add(e);
    }

    @Override
    public boolean contains(T e) {
        return list.contains(e);
    }

    @Override
    public void remove(T e) {
        list.remove(e);
    }
}
