package Classes;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class EclipseSetRep<T> implements InMemRep<T> {
    private UnifiedSet<T> set;

    public EclipseSetRep() {set = new UnifiedSet<>();}

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }
}
