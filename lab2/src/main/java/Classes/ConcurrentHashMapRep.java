package Classes;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRep<T> implements InMemRep<T> {
    private ConcurrentHashMap<T,T> chm;

    public ConcurrentHashMapRep() {chm=new ConcurrentHashMap<>();}

    @Override
    public void add(T e) {
        chm.put(e, e);
    }

    @Override
    public boolean contains(T e) {
        return chm.contains(e);
    }

    @Override
    public void remove(T e) {
        chm.remove(e);
    }
}
