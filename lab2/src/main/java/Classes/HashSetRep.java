package Classes;

import java.util.HashSet;

public class HashSetRep<T> implements InMemRep<T> {
    private HashSet<T> set;

    public HashSetRep() {set = new HashSet<>();}

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }
}
