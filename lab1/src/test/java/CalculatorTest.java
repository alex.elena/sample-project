

import org.junit.Before;
import org.junit.Test;
import service.Calculator;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    public Calculator calculator;

    @Test
    public void testAdd() {
        assertEquals(calculator.add(3,14),17);
    }
    @Test
    public void testSubtract() {
        assertEquals(calculator.subtract(3,14),-11);

    }

    @Test
    public void testMultiply() {
        assertEquals(calculator.multiply(3,4),12);

    }

    @Test
    public void testDivide() {
        assertEquals(calculator.divide(23,10),2);

    }

    @Before
    public void setUp() throws Exception {
        calculator= new Calculator();
    }
}