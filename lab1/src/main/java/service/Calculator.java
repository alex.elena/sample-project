package service;

import java.util.List;

public class Calculator {


    public int max(List<Integer> ints) {
        int result = ints.get(0);
        for (Integer n : ints) {
            if (n > result)
                result = n;
        }
        return result;
    }

    public int add(int numOne, int numTwo) {
        int result = numOne + numTwo;
        System.out.println(numOne + " + " + numTwo + " = " + result);
        return result;
    }

    public int subtract(int numOne, int numTwo) {
        int result = numOne - numTwo;
        System.out.println(numOne + " - " + numTwo + " = " + result);
        return result;
    }

    public int multiply(int numOne, int numTwo) {
        int result = numOne * numTwo;
        System.out.println(numOne + " * " + numTwo + " = " + result);
        return result;
    }

    public int divide(int numOne, int numTwo) {
        int result = numOne / numTwo;
        System.out.println(numOne + " / " + numTwo + " = " + result);
        return result;
    }

}