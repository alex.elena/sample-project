package service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CalculatorMain {
    private static Calculator calculator= new Calculator();

    public static void main(String[] args) {
        List<Integer> numbers= Arrays.stream(args).toList().stream().map(Integer::parseInt).collect(Collectors.toList());
        for (int i=0;i<numbers.size()-1;i++) {
            calculator.add(numbers.get(i),numbers.get(i+1));
            calculator.multiply(numbers.get(i),numbers.get(i+1));
            calculator.divide(numbers.get(i),numbers.get(i+1));
            calculator.subtract(numbers.get(i),numbers.get(i+1));
        }
        System.out.println(calculator.max(numbers));
    }
}
