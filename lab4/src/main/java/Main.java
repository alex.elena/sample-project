import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class Main {

    final static String INITIALISE_CONNECTION = "!hello";
    final static String CLOSE_CONNECTION = "!bye";
    final static String ACCEPT_CONNECTION = "!ack";
    final static String NEW_USER = "!write";
    final static String CLOSE_ALL = "!byebye";
    final static int BACKLOG = 50; // number of clients awaiting
    static ConcurrentHashMap<String, Socket> peers = new ConcurrentHashMap<>();
    static ConcurrentHashMap<String, BufferedWriter> writers = new ConcurrentHashMap<>();
    static volatile Scanner userInput = new Scanner(System.in);
    static volatile String myMessage;
    static String myName;


    static synchronized ServerSocket createServer() throws IOException {
        System.out.print("Insert address: ");
        final String ADDRESS = userInput.nextLine();

        System.out.print("Insert port: ");
        final int PORT = userInput.nextInt();
        userInput.nextLine();

        System.out.print("Insert an associated name: ");
        myName = userInput.nextLine();

        return new ServerSocket(PORT, BACKLOG, InetAddress.getByName(ADDRESS));
    }

    static Socket createEntry() throws IOException {
        System.out.print("Insert address: ");
        final String ADDRESS = userInput.nextLine();

        System.out.print("Insert port: ");
        final int PORT = userInput.nextInt();
        userInput.nextLine();

        return new Socket(ADDRESS, PORT);
    }

    static void handleListen() {
        try {
            ServerSocket serverSocket = createServer();
            Thread startServer = new Thread(() -> {
                while (true) {
                    Socket client;
                    try {
                        client = serverSocket.accept();
                        System.out.println("Client connected");

                        var messageReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                        var messageWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));

                        String clientMessage = messageReader.readLine();
                        System.out.println(clientMessage);

                        if (clientMessage.split(" ").length < 2) {
                            client.close();
                            continue;
                        }
                        String hisName = clientMessage.split(" ")[1];

                        if (clientMessage.startsWith(INITIALISE_CONNECTION)) {
                            peers.put(hisName, client);
                            writers.put(hisName, messageWriter);
                        } else {
                            System.out.println("Rejected connection");
                            client.close();
                            continue;
                        }

                        Thread readingThread = new Thread(() -> readFromClient(messageReader, messageWriter, hisName));
                        readingThread.start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            startServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFromClient(BufferedReader messageReader, BufferedWriter messageWriter, String hisName)  {
        boolean canRun = true;
        Optional<String> hisMessage;
        while (canRun) {
            try {
                hisMessage = Optional.ofNullable(messageReader.readLine());
                hisMessage.ifPresent(elem -> System.out.println("[ " + hisName + " ]: " + elem));
                if (hisMessage.isPresent() && hisMessage.get().startsWith(CLOSE_CONNECTION)) {
                    canRun = false;
                    System.out.println("CONNECTION CLOSED [ " + hisName + " ] CONNECTION CLOSED");
                    writers.get(hisName).close();
                    writers.remove(hisName);
                    messageWriter.close();
                    messageReader.close();
                    peers.get(hisName).close();
                    peers.remove(hisName);
                }
            } catch (IOException e) {
                System.out.println("CONNECTION CLOSED [ " + hisName + " ] CONNECTION CLOSED");
                canRun = false;
            }

        }
    }


    static void handleWrite() {
        System.out.print("Insert an associated name: ");
        String hisName = userInput.nextLine();
        Socket client;

        try {
            if (!peers.containsKey(hisName)) {
                client = createEntry();
                peers.put(hisName, client);
            }
            client = peers.get(hisName);

            System.out.println("Connecting to " + client.getLocalAddress());

            var messageReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            var messageWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));

            System.out.println("Sending !hello " + myName + " message...");

            messageWriter.write("!hello " + myName);
            messageWriter.newLine();
            messageWriter.flush();

            String clientMessage = messageReader.readLine();

            System.out.println("[ " + hisName + " ]: " + clientMessage);

            if (clientMessage.equals(ACCEPT_CONNECTION)) {
                writers.put(hisName, messageWriter);
                Thread readingThread = new Thread(() -> readFromClient(messageReader, messageWriter, hisName));
                readingThread.start();
            } else {
                System.out.println("Rejected");
                client.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        handleListen();
        int messageParts;
        while (true) {
            myMessage = userInput.nextLine();
            messageParts = myMessage.strip().split(" ").length;
            if (messageParts > 1) {
                String username = myMessage.strip().split(" ")[0];
                try {
                    if (writers.containsKey(username)) {
                        String message = Arrays.stream(myMessage.strip().split(" ")).skip(1).reduce("", String::concat);
                        BufferedWriter br = writers.get(username);
                        br.write(message);
                        br.newLine();
                        br.flush();
                        if(message.equals(CLOSE_CONNECTION) || message.equals(CLOSE_ALL)){
                            System.out.println("CONNECTION CLOSED [ " + username + " ] CONNECTION CLOSED");
                            writers.get(username).close();
                            writers.remove(username);
                            br.close();
                            peers.get(username).close();
                            peers.remove(username);
                        }
                    } else {
                        for (var br : writers.values()) {
                            br.write(myMessage);
                            br.newLine();
                            br.flush();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (messageParts == 1) {
                String command = myMessage.strip().split(" ")[0];
                switch (command) {
                    case NEW_USER -> handleWrite();
                    case CLOSE_CONNECTION, CLOSE_ALL -> {
                        try {
                            for (var br : writers.values()) {
                                br.write(command);
                                br.newLine();
                                br.flush();
                                br.close();
                            }
                            writers.clear();
                            for (var peer : peers.values()) {
                                peer.close();
                            }
                            peers.clear();
                            System.out.println("ALL CONNECTIONS CLOSED [ all ] ALL CONNECTIONS CLOSED");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    default -> {
                        for (var br : writers.values()) {
                            try {
                                br.write(myMessage);
                                br.newLine();
                                br.flush();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }
}
