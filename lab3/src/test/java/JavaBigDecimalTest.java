import junit.framework.TestCase;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class JavaBigDecimalTest extends TestCase {
    List<BigDecimal> decimals = new LinkedList<>();
    JavaBigDecimal JavDecimal ;

    public void testSum() {
        assertEquals(BigDecimal.valueOf(55),JavDecimal.sum());
    }

    public void testAverage() {
        assertEquals(BigDecimal.valueOf(6),JavDecimal.average());
    }

    public void testPrintTop10() {
        JavDecimal.printTop10();
        //trebuie sa printeze 10
    }

    public void setUp() throws Exception {
        super.setUp();
        for(int i=1;i<=10;i++)
            decimals.add(BigDecimal.valueOf( i));
        JavDecimal = new JavaBigDecimal(decimals);
    }
}