import java.io.*;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<BigDecimal> invoices = new LinkedList<>();
        List<BigDecimal> desList = new LinkedList<>();
        for(int i=0;i<150;i++)
        {
            invoices.add(BigDecimal.valueOf( (long)(Math.random() *1000000000)));
        }
        JavaBigDecimal JavDecimal = new JavaBigDecimal(invoices);

        // sum using stream
        BigDecimal sum = JavDecimal.sum();
        System.out.println("Sum = " + sum);
        BigDecimal avg= JavDecimal.average();
        System.out.println("Average = " + avg);
        JavDecimal.printTop10();

        try {
            FileOutputStream fileOut =new FileOutputStream("serials.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            for(int i=0;i<150;i++)
            {
                out.writeObject(invoices.get(i));
            }
            out.close();
            fileOut.close();
           // System.out.printf("Serialized data is saved in serials.ser\n");
        } catch (IOException i) {
            i.printStackTrace();
        }

        try {
            FileInputStream fileIn = new FileInputStream("serials.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            for(int i=0;i<150;i++)
            {
                desList.add((BigDecimal) in.readObject());
            }
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("BigDecimal class not found");
            c.printStackTrace();
            return;
        }
        JavaBigDecimal JavDecimal1 = new JavaBigDecimal(desList);
        BigDecimal sums = JavDecimal.sum();
        System.out.println("Sum des = " + sums);
        BigDecimal avgs= JavDecimal.average();
        System.out.println("Average des= " + avgs);
        JavDecimal.printTop10();
    }
}
