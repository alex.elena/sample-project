import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLOutput;
import java.util.*;

public class JavaBigDecimal {
    List<BigDecimal> decimals = new LinkedList<>();
    public JavaBigDecimal(List<BigDecimal> invoices) {
        decimals=invoices;
    }
    public JavaBigDecimal() {
    }

    public BigDecimal sum(){
        BigDecimal sum = decimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
        return sum;
    }
    public BigDecimal average(){
        BigDecimal sum = decimals.stream()
                .map(Objects::requireNonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return sum.divide(new BigDecimal(decimals.size()), RoundingMode.HALF_EVEN);
    }

    public void printTop10(){
        decimals.stream().sorted((Comparator.comparing(BigDecimal::doubleValue)).reversed()).limit(decimals.size()/10).forEach(System.out::println);
    }


}